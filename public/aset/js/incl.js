// memasukkan templat berdasarkan 'data-muat' //
$(function() {
  muat = $('[data-muat]')
  $.each(muat, function() {
    file = 'aset/html/' + $(this).data('muat') + '.html'
    $(this).load(file)
    $(this).removeAttr('data-muat')
  })
})

// masukkan templat kepala & kaki
$('header.utama').load('aset/html/kepala-utama.html');
$('footer.utama').load('aset/html/kaki-utama.html');
